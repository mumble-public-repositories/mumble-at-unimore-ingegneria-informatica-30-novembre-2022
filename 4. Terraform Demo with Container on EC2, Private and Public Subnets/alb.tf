# Application Load Balancer
resource "aws_lb" "alb_main" {
  name               = "alb-1"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.allow_http.id, aws_vpc.main.default_security_group_id]
  subnets            = [for subnet in aws_subnet.public_subnets : subnet.id]

  enable_deletion_protection = false
}

# Target Group
resource "aws_lb_target_group" "alb_target_group" {
  name        = "target-group-1"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.main.id

  tags = {
    Name = "TG-${var.project_name}-1"
  }

}

# Target Group Association
resource "aws_lb_target_group_attachment" "alb_target_group_association" {
  count = length(aws_subnet.private_subnets)

  target_group_arn = aws_lb_target_group.alb_target_group.arn
  target_id        = aws_instance.ec2_instances[count.index].private_ip
  port             = 80
}

# ALB Listener
resource "aws_lb_listener" "alb_listener_http" {
  load_balancer_arn = aws_lb.alb_main.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.alb_target_group.id
    type             = "forward"
  }
}
