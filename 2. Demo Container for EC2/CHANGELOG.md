# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.1](https://gitlab.com/mumble1/experiments/terraform-devops-script-demo-container/compare/v1.1.0...v1.1.1) (2022-11-11)


### Bug Fixes

* wrong apache user name ([4d63c11](https://gitlab.com/mumble1/experiments/terraform-devops-script-demo-container/commit/4d63c11aed037346683fa1c2b550bcdf6ee0c7fa))

## 1.1.0 (2022-11-11)


### Features

* project initialized ([196134b](https://gitlab.com/mumble1/experiments/terraform-devops-script-demo-container/commit/196134bdeb9f5aa1bbe0b0658ba980c1b55d7d3d))
