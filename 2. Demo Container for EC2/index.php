<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Terraform Demo</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">

    <!-- Google font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono&display=swap" rel="stylesheet">

    <!-- Style -->
    <style>
        body {
            font: 15px/1.5 "Roboto Mono", Helvetica, sans-serif;
            color: #ebebea;
            background: #222222;
            margin: 0;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
    </style>

    <?php
    $instance_id = file_get_contents(
        "http://169.254.169.254/latest/meta-data/instance-id"
    );
    ?>

</head>

<body class="h-100 text-left">
    <div class="p-3">
        <main class="px-3">
            <h1>Terraform demo Mumble</h1>
            <div>Instance ID:
                <?php echo htmlspecialchars($instance_id ?? "local", ENT_QUOTES, 'UTF-8'); ?>
            </div>

            <div>Instance IP:
                <?php echo htmlspecialchars($_SERVER['SERVER_ADDR'], ENT_QUOTES, 'UTF-8'); ?>
            </div>
        </main>
    </div>

</body>

</html>