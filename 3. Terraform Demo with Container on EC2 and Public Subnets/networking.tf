# Create VPC
resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "VPC-${var.project_name}"
  }
}

# Create public subnets
resource "aws_subnet" "public_subnets" {
  vpc_id                  = aws_vpc.main.id
  count                   = length(var.azs)
  cidr_block              = element(var.public_subnets, count.index)
  availability_zone       = element(var.azs, count.index)
  map_public_ip_on_launch = true

  tags = {
    Name = "Public Subnet ${count.index + 1}"
  }
}

# Create IGW
resource "aws_internet_gateway" "main-igw-01" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "Internet Gateway 01"
  }
}

# Single route table for public subnet
resource "aws_route_table" "public-rtable" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "Routing Table Public 01"
  }
}

# Add routes to public-rtable
resource "aws_route" "public-rtable" {
  route_table_id         = aws_route_table.public-rtable.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main-igw-01.id
}

# Route table association public subnets
resource "aws_route_table_association" "public-subnet-association" {
  count          = length(var.public_subnets)
  subnet_id      = element(aws_subnet.public_subnets.*.id, count.index)
  route_table_id = aws_route_table.public-rtable.id
}
