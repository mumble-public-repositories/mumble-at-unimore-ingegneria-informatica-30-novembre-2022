variable "project_name" {
  type        = string
  description = "Use a slugged name for the project"
  default     = "demo_unimore"
}

variable "aws_region" {
  type        = string
  description = "AWS Region"
  default     = "eu-central-1"
}

variable "aws_profile" {
  type        = string
  description = "Name of the local AWS Profile"
  default     = "unimore"
}

variable "azs" {
  description = "Availability Zones"
  type        = list(any)
  default     = ["eu-central-1a", "eu-central-1b"]
}

variable "vpc_cidr" {
  description = "VPC CIDR"
  type        = string
  default     = "123.0.0.0/16"
}

variable "public_subnets" {
  description = "Public Subnets CIDR"
  type        = list(any)
  default     = ["123.0.21.0/24", "123.0.22.0/24"]
}

variable "keypair" {
  description = "SSH Key"
  type        = map(any)
  default = {
    key_name   = "giacomo.torricelli.demo3"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCoRjx6zt8Ic4O6fA3fG6pTtWpv5bBp/50fO1vjUHxDLGzgODzFyYjMck9mX30Terd4ZnJKw1krPogBpzE4xaS4PIIiNCSmzUO65SNqv0tfvHU/iyCd+ATSk9pzdJnlH6GAV3IHhJR7ynBW4IJ95I3jNIOQoVKmglmX/guVqrGyahSIovaHKNnwN7OBbKqDpn5j5oDmgOcC7Kaw4b9S/H3mNpcWkhZ0bxY6zCMH/T2dYBmc23je1sVKCqG+oMxXoEE0OUsm7jbAoEReGkS7iEzkLxqsj9zLiCQzRXZi+3sWk14rs61JkWyYTEwdvBUjNnvLPa4/Wa+VJV8im40nXr7/ giacomo.torricelli@mumbleideas.it"
  }
}

variable "docker_registry" {
  description = "Docker Registry data"
  type        = map(any)
  default = {
    registry = "registry.gitlab.com"
    username = "giacomo.torricelli"
    password = "glpat-m-zd17597V6vhC_smSb4"
    image    = "registry.gitlab.com/mumble1/experiments/terraform-devops-script-demo-container"
  }
}
